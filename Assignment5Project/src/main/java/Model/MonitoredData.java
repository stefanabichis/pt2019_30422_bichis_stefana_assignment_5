package Model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class MonitoredData {
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activity;

	public MonitoredData() {

	}

	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public String toString() {
		return "Start time: " + this.startTime + ", end time: " + this.endTime + ", activity name: " + this.activity;
	}

	public String getDuration() {
		LocalDateTime tempDate = LocalDateTime.from(startTime);

		long hours = tempDate.until(endTime, ChronoUnit.HOURS);
		tempDate = tempDate.plusHours(hours);
		long minutes = tempDate.until(endTime, ChronoUnit.MINUTES);
		tempDate = tempDate.plusMinutes(minutes);
		long seconds = tempDate.until(endTime, ChronoUnit.SECONDS);
		tempDate = tempDate.plusSeconds(seconds);

		return hours + ":" + minutes + ":" + seconds;
	}

	public void printDuration() {
		System.out.println(this.getActivity() + " " + this.getDuration());
	}

	public double getTotalDuration() {
		LocalDateTime tempDate = LocalDateTime.from(startTime);

		long hours = tempDate.until(endTime, ChronoUnit.HOURS);
		tempDate = tempDate.plusHours(hours);
		long minutes = tempDate.until(endTime, ChronoUnit.MINUTES);
		tempDate = tempDate.plusMinutes(minutes);
		long seconds = tempDate.until(endTime, ChronoUnit.SECONDS);
		tempDate = tempDate.plusSeconds(seconds);
		double total = 60 * hours + minutes + seconds / 60;

		return total;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

}
