package Model;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;
import java.time.*;
import java.time.format.*;

public class Data {
	private ArrayList<MonitoredData> lists = new ArrayList<MonitoredData>();

	public ArrayList<MonitoredData> getLists() {
		return lists;
	}

	public void setLists(ArrayList<MonitoredData> lists) {
		this.lists = lists;
	}

	public void addData() throws IOException {
		Stream<String> stream = Files.lines(Paths.get("Activities.txt"));
		stream.forEach(line -> splitData(line));
	}

	private void splitData(String string) {
		String[] field = new String[3];
		field = string.split("\t\t");
		String activity = field[2];
		LocalDateTime startTime = LocalDateTime.parse(field[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		LocalDateTime endTime = LocalDateTime.parse(field[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		MonitoredData newData = new MonitoredData(startTime, endTime, activity);
		this.lists.add(newData);
	}

	public int days() {
		int days = (int) this.getLists().stream().map(date -> date.getStartTime().getDayOfYear()).distinct().count();
		return days;

	}

	public Map<String, Long> countActivities() {
		Map<String, Long> activities = this.getLists().stream()
				.collect(Collectors.groupingBy(activity -> activity.getActivity(), Collectors.counting()));
		return activities;
	}

	public Map<Integer, Map<String, Long>> countActivitiesPerDay() {
		Map<Integer, Map<String, Long>> activities = this.getLists().stream()
				.collect(Collectors.groupingBy(data -> data.getStartTime().getDayOfYear(),
						Collectors.groupingBy(activity -> activity.getActivity(), Collectors.counting())));
		return activities;
	}

	public void duration() {
		this.getLists().stream().forEach(a -> a.printDuration());
	}

	public double totalDuration() {
		double time = 0.0;
		for (MonitoredData data : this.lists) {
			time += data.getTotalDuration();
		}
		return time;
	}

	public Map<String, Double> durationPerActivity() {
		Map<String, Double> activity = this.getLists().stream().collect(Collectors
				.groupingBy(data -> data.getActivity(), Collectors.summingDouble(MonitoredData::getTotalDuration)));
		return activity;
	}

	public ArrayList<String> U5min() {
		ArrayList<String> result = new ArrayList<String>();
		Map<String, Long> total = this.countActivities();
		Map<String, Long> underFive = this.getLists().stream().filter(activity -> {
			return activity.getTotalDuration() < 5;
		}).collect(Collectors.groupingBy(data -> data.getActivity(), Collectors.counting()));

		for (Map.Entry<String, Long> total2 : total.entrySet()) {
			for (Map.Entry<String, Long> underFive2 : underFive.entrySet()) {
				if (underFive2.getKey().equals(total2.getKey())) {
					double per = underFive2.getValue() / total2.getValue();
					if (per > 0.9)
						result.add(underFive2.getKey());
				}
			}
		}

		return result;
	}
}
