package PT2019.assignment5.Assignment5Project;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Map;

import Model.Data;

public class App {

	public static void main(String[] args) {
		Data data = new Data();
		System.out.println("DATA FROM TEXT FILE:");
		try {
			data.addData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Distinct days in text file:");
		
		System.out.println(data.days());

		System.out.println("Activity" + "   " + "Number of aparitions");
		Map<String, Long> countactivivitie = data.countActivities();
		for (Map.Entry<String, Long> entry : countactivivitie.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}

		Map<Integer, Map<String, Long>> day = data.countActivitiesPerDay();
		for (Map.Entry<Integer, Map<String, Long>> entry : day.entrySet()) {
			System.out.println("Day of the year" + " " + entry.getKey() + " ");
			Map<String, Long> activity = entry.getValue();
			for (Map.Entry<String, Long> entry2 : activity.entrySet()) {
				System.out.println("Activity" + "   " + "Number of aparitions in one day");
				System.out.println(entry2.getKey() + " " + entry2.getValue());
			}
		}
		System.out.println("Activity" + "   " + "Duration");
		data.duration();

		System.out.println("Activity" + "   " + "Total duration in minutes");
		Map<String, Double> totalduration = data.durationPerActivity();
		for (Map.Entry<String, Double> entry : totalduration.entrySet()) {
			double result = entry.getValue();
			NumberFormat formatter = new DecimalFormat("#0.000");
			System.out.println(entry.getKey() + " " + formatter.format(result));
		}

		ArrayList<String> u5duration = data.U5min();
		System.out.println("Activities that have 90% of the monitoring records with duration less than 5 minutes ");
		for (String entry : u5duration) {
			System.out.println(entry);
		}

	}
}
